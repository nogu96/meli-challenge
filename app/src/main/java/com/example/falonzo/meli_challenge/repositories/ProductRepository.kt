package com.example.falonzo.meli_challenge.repositories

import com.example.falonzo.meli_challenge.api.SearchApi

class ProductRepository(
    private val searchApi: SearchApi
) {

    fun search(input: String) = searchApi.search(input)

}