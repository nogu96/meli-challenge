package com.example.falonzo.meli_challenge.model

import java.io.Serializable

/**
 * A generic class that holds a value with its loading status.
 * @param <T>
</T> */
data class Resource<out T>(val status: Status, val data: T?, val message: String?): Serializable {
    companion object {
        fun <T> success(data: T?): Resource<T> {
            return Resource(Status.SUCESS, data, null)
        }

        fun <T> error(msg: String, data: T?): Resource<T> {
            return Resource(Status.ERROR, data, msg)
        }

        fun <T> loading(data: T?): Resource<T> {
            return Resource(Status.LOADING, data, null)
        }
    }
}

enum class Status {
    SUCESS,
    ERROR,
    LOADING
}