package com.example.falonzo.meli_challenge.ui.searchFragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.falonzo.meli_challenge.databinding.FragmentSearchBinding
import com.example.falonzo.meli_challenge.model.Publication
import com.example.falonzo.meli_challenge.model.Status
import com.example.falonzo.meli_challenge.ui.adapters.PublicationsAdapter
import org.koin.androidx.viewmodel.ext.android.viewModel


class SearchFragment : Fragment(), PublicationsAdapter.Listener {

    private lateinit var binding: FragmentSearchBinding
    private val viewModel: SearchViewModel by viewModel()
    private val publicationAdapter = PublicationsAdapter()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSearchBinding.inflate(LayoutInflater.from(context), container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        publicationAdapter.setListener(this)
        binding.recycler.apply {
            adapter = publicationAdapter
            layoutManager = LinearLayoutManager(context)
        }

        binding.searchToolbar.userInput.observe(viewLifecycleOwner) { input ->
            if (input.isNullOrEmpty())
                return@observe

            viewModel.search(input).observe(viewLifecycleOwner) { response ->
                when(response.status) {
                    Status.SUCESS -> {
                        response.data?.let { publicationList ->
                            if (publicationList.isEmpty()) {
                                binding.recycler.isVisible = false
                                binding.progressBar.isVisible = false
                                return@let
                            }
                            binding.recycler.isVisible = true
                            binding.progressBar.isVisible = false
                            publicationAdapter.load(publicationList)
                        }
                    }
                    Status.ERROR -> {
                        binding.recycler.isVisible = false
                        binding.progressBar.isVisible = false
                    }
                    Status.LOADING -> {
                        binding.recycler.isVisible = false
                        binding.progressBar.isVisible = true
                    }
                }
            }
        }
    }

    override fun onSelect(publication: Publication) {
        val action = SearchFragmentDirections.actionSearchFragmentToDetailFragment(publication)
        findNavController().navigate(action)
    }

}