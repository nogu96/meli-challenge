package com.example.falonzo.meli_challenge.ui.detailFragment

import android.content.Context
import android.util.AttributeSet

import android.view.LayoutInflater

import android.widget.LinearLayout
import com.example.falonzo.meli_challenge.R
import com.example.falonzo.meli_challenge.databinding.AtributeItemBinding
import com.example.falonzo.meli_challenge.model.Attribute
import java.lang.String


class DetailLayout @JvmOverloads constructor(context : Context,
                                             attributeSet: AttributeSet? = null,
                                             defStyle: Int = 0)
    : LinearLayout(context, attributeSet, defStyle) {

    init {
        orientation = VERTICAL
    }

    fun setAttributesList(attributeList: List<Attribute>) {
        attributeList.forEach { attribute ->
            val binding = AtributeItemBinding.inflate(LayoutInflater.from(context), this, false)
            binding.name.text = String.format(context.getString(R.string.attribute_name), attribute.name)
            binding.value.text = attribute.valueName
            addView(binding.root)
        }
    }

}
