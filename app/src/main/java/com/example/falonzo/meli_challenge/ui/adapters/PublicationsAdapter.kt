package com.example.falonzo.meli_challenge.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.falonzo.meli_challenge.R
import com.example.falonzo.meli_challenge.databinding.PublicationItemBinding
import com.example.falonzo.meli_challenge.extensions.load
import com.example.falonzo.meli_challenge.model.Publication

class PublicationsAdapter: RecyclerView.Adapter<PublicationsAdapter.ViewHolder>() {

    private val publicationList: MutableList<Publication> = mutableListOf()
    private var listener: Listener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            PublicationItemBinding.inflate(LayoutInflater.from(parent.context), parent, false),
            listener
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(publicationList[position])
    }

    override fun getItemCount() = publicationList.size

    fun load(list: List<Publication>) {
        publicationList.clear()
        publicationList.addAll(list)
        notifyDataSetChanged()
    }

    fun setListener(listener: Listener) {
        this.listener = listener
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        super.onDetachedFromRecyclerView(recyclerView)
        listener = null
    }

    class ViewHolder(
        private val binding: PublicationItemBinding,
        private val listener: Listener?
    ): RecyclerView.ViewHolder(binding.root) {

        private val context = binding.root.context

        fun bind(publication: Publication) {
            binding.title.text = publication.title
            binding.image.load(publication.thumbnail)
            binding.price.text = String.format(context.getString(R.string.price) , publication.price.toString())
            binding.zone.text = publication.address.cityName
            binding.root.setOnClickListener {
                listener?.onSelect(publication)
            }
        }
    }

    interface Listener {
        fun onSelect(publication: Publication)
    }
}