package com.example.falonzo.meli_challenge.extensions

import android.widget.ImageView
import com.bumptech.glide.Glide

fun ImageView.load(url: String?) {
    url?.let {
        Glide.with(context)
            .load(it)
            .into(this)
    }
}