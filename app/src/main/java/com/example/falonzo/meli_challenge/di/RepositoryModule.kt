package com.example.falonzo.meli_challenge.di

import com.example.falonzo.meli_challenge.repositories.ProductRepository
import org.koin.dsl.module

val repositoryModule = module {

    single { ProductRepository(get()) }

}