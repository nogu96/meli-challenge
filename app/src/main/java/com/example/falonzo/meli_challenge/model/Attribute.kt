package com.example.falonzo.meli_challenge.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Attribute(val name: String? = null,
                @SerializedName("value_name")
                val valueName: String? = null,
                val id: String? = null,
                val attributeGroupId: String? = null,
                val attributeGroupName: String? = null,
                val source: Long? = null,
) : Serializable