package com.example.falonzo.meli_challenge.ui.searchFragment

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.core.widget.doAfterTextChanged
import androidx.core.widget.doOnTextChanged
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.falonzo.meli_challenge.databinding.SearchToolbarBinding
import java.util.*

class SearchToolbar @JvmOverloads constructor(
    context : Context,
    attrs : AttributeSet? = null,
    defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr)  {

    private val DELAY = 600L
    private val binding = SearchToolbarBinding.inflate(LayoutInflater.from(context), this, true)
    private var timer: Timer? = null
    private val _userInput = MutableLiveData<String>()
    val userInput: LiveData<String> = _userInput

    init {
        binding.searchText.apply {
            doOnTextChanged { text, _, _, _ ->
                timer?.cancel()
            }
            doAfterTextChanged {
                if(!this.hasFocus()) {
                    return@doAfterTextChanged
                }
                timer = Timer()
                timer?.schedule(object : TimerTask() {
                    override fun run() {
                        _userInput.postValue(it.toString())
                    }
                }, DELAY)
            }
        }
    }

}