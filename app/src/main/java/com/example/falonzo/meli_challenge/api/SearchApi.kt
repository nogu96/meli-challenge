package com.example.falonzo.meli_challenge.api

import androidx.lifecycle.LiveData
import com.example.falonzo.meli_challenge.model.SearchResponseBody
import com.example.falonzo.meli_challenge.network.ApiResponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface SearchApi {

    @GET("search")
    fun search(@Query("q") input: String): LiveData<ApiResponse<SearchResponseBody>>

}