package com.example.falonzo.meli_challenge.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.falonzo.meli_challenge.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}