package com.example.falonzo.meli_challenge.ui.detailFragment

import android.R
import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater

import android.widget.LinearLayout
import com.example.falonzo.meli_challenge.databinding.DetailToolbarBinding


class DetailToolbar@JvmOverloads constructor(context : Context,
                                             attributeSet: AttributeSet? = null,
                                             defStyle: Int = 0)
    : LinearLayout(context, attributeSet, defStyle) {

    private val binding = DetailToolbarBinding.inflate(LayoutInflater.from(context), this, true)
    private var listener: Listener? = null

    init {
        binding.backArrow.setOnClickListener {
            listener?.onBackButtonPress()
        }
    }

    fun setListener(listener: Listener?) {
        this.listener = listener
    }

    interface Listener {
        fun onBackButtonPress()
    }


}