package com.example.falonzo.meli_challenge.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class PublicationAddress(
    @SerializedName("city_name")
    val cityName: String
): Serializable
