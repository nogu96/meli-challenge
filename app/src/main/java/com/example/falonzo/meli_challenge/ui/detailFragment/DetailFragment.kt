package com.example.falonzo.meli_challenge.ui.detailFragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import com.example.falonzo.meli_challenge.R
import com.example.falonzo.meli_challenge.databinding.FragmentDetailBinding
import com.example.falonzo.meli_challenge.extensions.load
import org.koin.androidx.viewmodel.ext.android.viewModel
import android.content.Intent
import android.net.Uri


class DetailFragment : Fragment() {

    private lateinit var binding: FragmentDetailBinding
    private val viewModel: DetailViewModel by viewModel()
    private val navArgs by navArgs<DetailFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDetailBinding.inflate(LayoutInflater.from(context), container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navArgs.publication.let { publication ->
            binding.title.text = publication.title
            binding.image.load(publication.thumbnail)
            binding.price.text = getString(R.string.price, publication.price.toString())
            binding.place.text = publication.address.cityName
            binding.characteristicsLayout.setAttributesList(publication.attributes)
        }

        binding.toolbar.setListener(object : DetailToolbar.Listener {
            override fun onBackButtonPress() {
                activity?.onBackPressed()
            }
        })

        binding.openMeli.setOnClickListener {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(navArgs.publication.permalink))
            startActivity(browserIntent)
        }
    }

}