package com.example.falonzo.meli_challenge.ui.searchFragment

import androidx.lifecycle.*
import com.example.falonzo.meli_challenge.model.Publication
import com.example.falonzo.meli_challenge.model.Resource
import com.example.falonzo.meli_challenge.network.ApiSuccessResponse
import com.example.falonzo.meli_challenge.repositories.ProductRepository

class SearchViewModel(
    private val productRepository: ProductRepository
): ViewModel() {

    fun search(input: String): LiveData<Resource<List<Publication>>> {
        val mediator = MediatorLiveData<Resource<List<Publication>>>()
        mediator.value = Resource.loading(null)
        mediator.addSource(productRepository.search(input)) { response ->
            when(response) {
                is ApiSuccessResponse -> {
                    mediator.value = Resource.success(response.body.results)
                } else -> {
                    mediator.value = Resource.error("", null)
                }
            }
        }
        return mediator
    }

}