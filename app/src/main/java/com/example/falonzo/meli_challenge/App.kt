package com.example.falonzo.meli_challenge

import android.app.Application
import com.example.falonzo.meli_challenge.di.apiModule
import com.example.falonzo.meli_challenge.di.appModule
import com.example.falonzo.meli_challenge.di.repositoryModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class App: Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin{
            androidLogger()
            androidContext(this@App)
            modules(appModule, apiModule, repositoryModule)
        }
    }
}