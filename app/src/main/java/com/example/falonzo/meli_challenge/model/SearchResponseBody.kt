package com.example.falonzo.meli_challenge.model

data class SearchResponseBody(
    val results: List<Publication>? = null
)