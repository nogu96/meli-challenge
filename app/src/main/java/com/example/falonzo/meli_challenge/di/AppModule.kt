package com.example.falonzo.meli_challenge.di

import com.example.falonzo.meli_challenge.ui.detailFragment.DetailViewModel
import com.example.falonzo.meli_challenge.ui.searchFragment.SearchViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {

    viewModel { SearchViewModel(get()) }
    viewModel { DetailViewModel() }

}