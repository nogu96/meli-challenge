package com.example.falonzo.meli_challenge.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Publication(
    val id: String,
    val title: String,
    val price: Float,
    val condition: Condition,
    val thumbnail: String,
    val address: PublicationAddress,
    val attributes: List<Attribute>,
    val permalink: String,
    @SerializedName("catalog_product_id")
    val catalogProductId: String,
    @SerializedName("category_id")
    val categoryId: String,
    @SerializedName("original_price")
    val originalPrice: Float,
): Serializable

enum class Condition {
    @SerializedName("new")
    NEW,
    @SerializedName("used")
    USED
}